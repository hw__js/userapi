// example link to run
import { getInfoFromForm } from './../controller/productController.js';
const BASE_URL = "https://6360e782af66cc87dc1d5150.mockapi.io";

// official link to deploy
// const BASE_URL = "https://633ec0420dbc3309f3bc527c.mockapi.io";

export let getListProductServ = () => {
  return axios({
    // example link
    url: `${BASE_URL}/api/v1/phuchodien/products`,

    // official link
    // url: `${BASE_URL}/Products`,
    method: "GET",
  });
};

export let postCreateProductServ = (products) => {
  return axios({
    // example link
    url: `${BASE_URL}/api/v1/phuchodien/products`,

    // official link
    // url: `${BASE_URL}/Products`,
    method: "POST",
    data: products,
  });
};

export let deleteProductIdServ = (id) => {
  return axios({
    // example link
    url: `${BASE_URL}/api/v1/phuchodien/products/${id}`,

    // official link
    // url: `${BASE_URL}/Products/${id}`,
    method: "DELETE",
  });
};

export let editProductServ = (id) => {
  return axios({
    // example link
    url: `${BASE_URL}/api/v1/phuchodien/products/${id}`,

    // official link
    // url: `${BASE_URL}/Products/${id}`,
    method: "PUT",
  });
};




