// Lấy thông tin từ form
export let getInfoFromForm = () => {
  const name = document.getElementById("TenSP").value;
  const price = document.getElementById("GiaSP").value;
  const image = document.getElementById("HinhSP").value;
  const desc = document.getElementById("MoTa").value;

  return {
    name: name,
    price: price,
    img: image,
    desc: desc,
  };
};

// show sản phẩm lên form
export let showInforOnForm = (product) => {
  document.getElementById("TenSP").value = product.name;
  document.getElementById("GiaSP").value = product.price;
  document.getElementById("HinhSP").value = product.img;
  document.getElementById("MoTa").value = product.desc;
};

export let renderProductList = (list) => {
  let contentHTML = "";

  list.forEach((item) => {
    contentHTML += `<tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td class="p-2"> ${item.price} $ </td>
        <td> 
        <img style="width: 120px" id = "${item.id}"  src="${item.image}"/> 
   
        </td>
        <td>${item.desc}</td>
        <td>      
            <button onclick = "deleteProduct(${item.id})" class = "btn btn-danger">Xóa</button>
            <button onclick = "editProduct(${item.id})" class = "btn btn-primary" data-toggle="modal"
            data-target="#myModal">Sửa</button>
        </td>
        </tr>`;
  });

  document.getElementById("tblDanhSachSP").innerHTML = contentHTML;
};

export let resetForm = () => {
  document.getElementById("reset").reset();
}
