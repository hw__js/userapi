export let checkEmpty = (value, idErr) => {
    if(value.length == 0) {
        document.getElementById(idErr).innerText = "This field cannot be left blank";
        return false;
    } else {
        document.getElementById(idErr).innerText = "";
        return true;
    }
}

export let checkPrice = (value, idErr) => {
    if(value > 1000 & value < 5000) {
        document.getElementById(idErr).innerText = "";
        return true;
    } else {
        document.getElementById(idErr).innerText = "Price must from 1000 $ to 5000 $";
        return false;
    }
}