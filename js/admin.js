import { Products } from "./model/productModel.js";
import { productsData } from "./controller/products-data.js";
import { checkEmpty, checkPrice } from "./controller/validation.js";
import {
  getInfoFromForm,
  renderProductList,
  resetForm,
  showInforOnForm,
} from "./controller/productController.js";
import {
  postCreateProductServ,
  getListProductServ,
  deleteProductIdServ,
  editProductServ,
} from "./serv/productServ.js";

const turnOnLoading = () => {
  document.getElementById("loading").style.display = "flex";
};

const turnOffLoading = () => {
  document.getElementById("loading").style.display = "none";
};

//  render ra giao diện
let renderListProductServ = () => {
  turnOnLoading();
  getListProductServ()
    .then((res) => {
      turnOffLoading();
      let productsList = res.data.map((item) => {
        return new Products(
          item.id,
          item.name,
          item.price,
          item.img,
          item.desc
        );
      });

      renderProductList(productsList);
    })
    .catch((err) => {
      turnOffLoading();
    });
};

// start render
renderListProductServ();

// thêm sản phẩm(
document.getElementById("addProducts").onclick = () => {
  turnOnLoading();
  let dataForm = getInfoFromForm();

  // Validate

  postCreateProductServ(dataForm)
    .then((res) => {
      turnOffLoading();
      // validate
      let isValid = true;
      isValid &= checkEmpty(dataForm.name,"tbTen");
      isValid &= checkEmpty(dataForm.price, "tbGia") && checkPrice(dataForm.price, "tbGia");
      isValid &= checkEmpty(dataForm.img, "tbHinhSp");
      if(isValid) {
        renderListProductServ();
        Swal.fire("Successfull");
      }
    })
    .catch((err) => {
      Swal.fire("Fail");
    });
    resetForm();
};

// Xóa sản phẩm
let deleteProduct = (idProduct) => {
  turnOnLoading();
  deleteProductIdServ(idProduct)
    .then((res) => {
      turnOffLoading();
      renderListProductServ();
      Swal.fire("Successfull");
    })
    .catch((err) => {
      Swal.fire("Fail");
    });
};
window.deleteProduct = deleteProduct;

// SỬa sản phẩm
let editProduct = (idProduct) => {
  editProductServ(idProduct)
    .then((res) => {
      showInforOnForm(res.data);
      renderListProductServ();
    })
    .catch((err) => {
      console.log("err: ", err);
    });
};
window.editProduct = editProduct;

// Cập nhật
document.getElementById("btnCapNhat").onclick = () => {
  let product = getInfoFromForm();
  
  axios({
    url: `https://633ec0420dbc3309f3bc527c.mockapi.io/api/v1/phuchodien/products/product/${product.id}`,
    method: "PUT",
    data: product,
  }).then( (res) => {
    console.log('res: ', res.data);
    renderListProductServ();
  }).catch( (err) => {
    console.log('err: ', err);
  })
}
// search Product

// let searchProducts = (value, listProduct) => {
//   getListProductServ().then( (res) => {
//     var filterProducts = [];
//     for (let i = 0; i < listProduct.length; i++) {
//       value = value.toLowerCase();
//       let typeOfProduct = listProduct[i].name.toLowerCase();
//       if (typeOfProduct.includes(value)) {
//         filterProducts.push(listProduct[i]);
//       }
//     }
//     return filterProducts;
//   }).catch((err) => {
//     console.log('err: ', err);
//   })
// };

// document.getElementById("btnSearch").onclick = function () {
//   let valueSelect = document.getElementById("inputTK").value;
//   console.log('valueSelect: ', valueSelect);
//   let searchProduct = searchProducts(valueSelect, productsData);
//   if(searchProduct) {
//     renderListProductServ(searchProduct)
//   } else {
//     renderListProductServ(productsData);
//   }
//   console.log('searchProduct: ', searchProduct);
// };

